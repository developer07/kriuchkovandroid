fun interface FileWriter <T>{
    fun writingData(fileName: String, data: Iterable<T>)
}