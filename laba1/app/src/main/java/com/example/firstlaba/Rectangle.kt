package com.example.firstlaba

class Rectangle(color: String, val width: Double, val height: Double) : GeometricShape(color) {
    override fun area(): Double {
        return width * height
    }

    override fun perimeter(): Double {
        return 2 * (width + height)
    }
}
