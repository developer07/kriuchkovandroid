package com.example.firstlaba


import org.junit.Assert
import org.junit.Test
import org.junit.Assert.*

class ShapeUtilTest {

    @Test
    fun testCalculateTotalArea() {
        // Создание фигур
        val circle = Circle("red", 5.0, )
        val rectangle = Rectangle("blue", 3.0, 4.0, )
        val triangle = Triangle("green", 3.0, 4.0, 5.0, )

        // Вызов метода calculateTotalArea для проверки правильности вычисления общей площади
        val totalArea = ShapeUtil.calculateTotalArea(arrayOf(circle, rectangle, triangle))

        // Предполагаемое значение общей площади, которое можно проверить
        val expectedTotalArea = circle.area() + rectangle.area() + triangle.area()

        // Утверждение для сравнения рассчитанной и ожидаемой общей площади
        Assert.assertEquals(expectedTotalArea, totalArea, 0.001) // 0.001 - это погрешность для сравнения вещественных чисел
    }
}


