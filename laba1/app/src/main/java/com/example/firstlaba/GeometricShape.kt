package com.example.firstlaba

abstract class GeometricShape(val color: String) {
    abstract fun area(): Double
    abstract fun perimeter(): Double
}

