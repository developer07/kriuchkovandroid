package com.example.firstlaba

class Triangle(color: String, val side1: Double, val side2: Double, val side3: Double) : GeometricShape(color) {
    override fun area(): Double {
        val s = (side1 + side2 + side3) / 2
        val area = Math.sqrt(s * (s - side1) * (s - side2) * (s - side3))
        return area
    }

    override fun perimeter(): Double {
        return side1 + side2 + side3
    }
}
