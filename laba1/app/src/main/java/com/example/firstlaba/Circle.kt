package com.example.firstlaba

class Circle(color: String, val radius: Double) : GeometricShape(color) {
    override fun area(): Double {
        return Math.PI * radius * radius
    }

    override fun perimeter(): Double {
        return 2 * Math.PI * radius
    }
}
