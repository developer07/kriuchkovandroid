package com.example.firstlaba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.example.firstlaba.ShapeUtil.calculateTotalArea

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val circle = Circle("red", 5.0, )
        val rectangle = Rectangle("blue", 3.0, 4.0, )
        val triangle = Triangle("green", 3.0, 4.0, 5.0, )

        val totalArea = ShapeUtil.calculateTotalArea(
            arrayOf(circle, rectangle, triangle))

        val resultTextView: TextView = findViewById(R.id.resutlTextView)
        resultTextView.text = "Total area of all shapes: $totalArea"
    }
}