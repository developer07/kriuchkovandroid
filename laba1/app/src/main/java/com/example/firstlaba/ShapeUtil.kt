package com.example.firstlaba

object ShapeUtil {
    fun calculateTotalArea(shapes: Array<GeometricShape>): Double {
        var totalArea = 0.0
        for (shape in shapes) {
            totalArea += shape.area()
        }
        return totalArea
    }
}
